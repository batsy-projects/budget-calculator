def print_budget_plan(item, cost):
    # Always write proper docstrings and comments.
    """
    Just a sample function to print the item and its cost.

    :type item: str
    :param item: The item you want to buy

    :type cost: int
    :param cost: The cost of the `item`

    :rtype: None
    :returns: Nothing. Just print a message.
    """

    # `f""` is called `f-string` available in Python 3+  
    print(f"{item} will cost you Rs. {cost}.")

# Search more about `if __name__ == "__main__"` syntax from the Web.
if __name__ == "__main__":
    # Take input from the user 
    item = input("Which item do you want to buy?...")
    cost = input(f"How much will {item} cost you?...")

    item = item.strip()
    # `strip()` is available for all string objects. It strips off any leading and trailing whitespaces.
    
    # Validation: 1
    # Check if the `item` is an empty string.
    if not item:
        print(f"'item' cannot be empty!")
        # Terminate the program!
        exit()

    # Validation: 2
    # Validation using exception handling. Check if `cost` is an integer or not.
    try:
        # Type conversion or casting.
        cost = int(cost)
    # Always specify the exact error you are expecting in the `try` block.
    except ValueError:
        print(f"'cost' must be an integer - not {cost}")
        exit()

    # If the execution has reached this line, that means all validations were passed. All inputs are valid. 
    # So you can call budget function.
    print_budget_plan(item=item, cost=cost)
    # fucntion call by passing `kwargs`. Read more about `args` and `kwargs` on the Web.
    
    #Budget Calculator
print(" \n\t     _____HELLO_____ \n *****WELCOME TO BUDGET CALCULATOR*****\n")

item=str(input("I want to buy : "))
item = item.strip()
if not item:
        print(f"'item' cannot be empty!")
        exit()
try:      
    item = str(item)    
except ValueError:
     print(f"'item' must be a string - not {item}")
     exit()

cost=int(input("Cost of the {} is : Rs. ".format(item)))
if not cost:
        print(f"'cost' cannot be empty!")
        exit()
try:      
    cost = int(cost)    
except ValueError:
     print(f"'cost' must be an integer - not {cost}")
     exit()


print("\n Let's check how much money you got to buy a {}!!\n".format(item))

def budget(m1,m2,m3):
    total=m1+m2+m3
    if total>cost:
        print("\nHurray!!, You have Rs.{}. \nYou can buy a {} now. \nGO GET IT\n".format(total,item))
        print("Your balance is {}".format(total-cost))
    else:
        print("\nSorry!!, You have Rs.{}.\n You still need Rs.{}.\n You must wait for some more time to buy a {}\n".format(total,(cost-total),item))
        return

gift=int(input("Gift Money from Family : Rs. "))
if not gift:
        print(f"'gift' cannot be empty!")
        exit()
try:      
    gift = int(gift)    
except ValueError:
     print(f"'gift' must be an integer - not {gift}")
     exit()
savings=int(input("Savings from Pocket Money : Rs. "))
if not savings:
        print(f"'savings' cannot be empty!")
        exit()
try:      
    savings = int(savings)    
except ValueError:
     print(f"'savings' must be an integer - not {savings}")
     exit()
internship=int(input("Stepend earned from Internship : Rs. "))
if not internship:
        print(f"'internship' cannot be empty!")
        exit()
try:      
    internship = int(internship)    
except ValueError:
     print(f"'internship' must be an integer - not {internship}")
     exit()
total=gift+savings+internship

budget(gift,savings,internship)

